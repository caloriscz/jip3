<?php
namespace App\Controller;

use App\Entity\Links;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class LuckyController extends Controller
{
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function number()
    {
        $links = $this->getDoctrine()->getRepository(Links::class)->findAll();

        if (!$links) {
            throw $this->createNotFoundException('No lucky found for id '. 1);
        }

        return $this->render('lucky/links.html.twig', [
            'links' => $links
        ]);
    }
}