<?php
namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HomepageController extends Controller
{

    public function default()
    {
        return $this->render('default.html.twig', [
        ]);
    }
}