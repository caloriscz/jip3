<?php
namespace App\Controller;

use App\Entity\Links;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;

class LinksController extends Controller
{
    public function show()
    {
        // creates a task and gives it some dummy data for this example

        $task = new Links();
        $task->setTitle('Yahoo.com');
        $task->setUrl('http://yahoo.com');

        $form = $this->createFormBuilder($task)
            ->add('title', TextType::class)
            ->add('url', TextType::class)
            ->add('save', SubmitType::class, ['label' => 'Create Task'])
            ->getForm();

        return $this->render('links/form.html.twig', [
            'form' => $form->createView(),
        ]);
    }
}
